import React from 'react';
import { faInstagram, faFacebook, faYoutube} from '@fortawesome/free-brands-svg-icons';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import Network from './network';
import Info from './contactInfo';

function Contact(){

    return(
        <div className="contact" id="contact">
            <div className="contact-box">
                {/* <div className="message">
                    <h1>Sigueme para mas tips. </h1>
                </div> */}
                <div className="networks">
                    <Network togo="https://www.instagram.com/cecibumakeup/"icon={faInstagram}/>
                    <Network togo="https://www.facebook.com/cecibumakeup"icon={faFacebook}/>
                    <Network togo="https://www.youtube.com/channel/UC000ReYBYrQj4RXhckrPQ-g/featured?app=desktop"icon={faYoutube}/>
                </div>
                <div className="emailphone">
                    <Info linkname="cecibumakeup@gmail.com" togo="mailto:cecibumakeup@gmail.com" icon={faEnvelope} />
                    <Info linkname="+502 5123-4567" togo="#" icon={faPhoneAlt} />
                </div>
            </div>
        </div>
    );
}

export default Contact;