import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function Info(props){
    
    return(
        <div className="info-area">
                <a href={props.togo} className="info-link">
                <i><FontAwesomeIcon icon={props.icon} /></i>
                <span>{props.linkname}</span>
                </a>
        </div>
    );
}

export default Info;
