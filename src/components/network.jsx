import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function Network(props){

    return(
        <div className="network">
                <a href={props.togo} className="network-link">
                <i><FontAwesomeIcon icon={props.icon} /></i>
                {/* <span>{props.section}</span> */}
                </a>
        </div>
    );
}

export default Network;