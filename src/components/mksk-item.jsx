import React from 'react';

function MakeupItem (props){

            let clas = 'box-item';
            clas = clas + ' ' + props.image;

        
    return(
        <div className={clas}>
            <div className="item-photo"></div>
            <div className="item-text">
                <h3 className="title">{props.title}</h3>
                <p className="box-text">{props.text}</p>
            </div>
        </div>
    );
}

export default MakeupItem;