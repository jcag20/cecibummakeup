import React from 'react';

function About (){
    return(
        <div className='about' id="about">
            <div className="about-box">
                <div className="about-photo">

                </div>
                <div className="about-description">
                    <div className="about-title">
                        <h1>Acerca de mí</h1>
                    </div>
                    <div className="about-text">
                        <p>
                        Me gusta pensar que la mejor manera de sentirme bien y estar feliz, es cuidar de mí misma primero. 
                        <br/><br/> Soy maquillista y asesora de imagen, y estoy aquí para compartir con ustedes tips, videos, tutoriales y muchas ideas que espero les sean muy útiles en su día a día. 
                        <br/> <br/> Tengo la intención de formar una comunidad en donde fomentemos el amor propio y el empoderamiento, para así comenzar a conocer nuestra mejor versión. 
                        <br/><br/>
                        Tu primer amor, eres tú ♡
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default About;