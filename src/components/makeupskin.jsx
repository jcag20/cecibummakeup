import React from 'react';
import MakeupItem from './mksk-item';

function MakeupSkin(){
    return(
        <div className="makeupskin" id="makeupskin">
            <div className="makeup-box">
                <MakeupItem  
                    image="photo-mk" 
                    title="Make Up Ideas"
                    text="Cuando se trata de maquillaje, lo más importante no es aplicarnos una gran cantidad de maquillaje. Más bien hay que pensar las técnicas que vamos a utilizar para poder realizarnos un buen maquillaje, un maquillaje correctivo. 
                    El maquillaje correctivo lo que busca es acentuar las partes que más nos gustan de nuestro rostro. En esta sección te daré los tips más importantes para maquillar tu rostro de una manera correctiva y estéticamente armoniosa."
                />
                <MakeupItem  
                    image="photo-sk" 
                    title="Skin Care"
                    text="Para empezar a cuidar de tu piel, lo más importante es aprender a conocerla e identificarla. Hay diferentes tipos de piel, hay piel grasa, piel seca, piel mixta, piel deshidratada y piel sensible. Cada una de ellas tiene diferentes características. Por ejemplo, la piel mixta es de las más comunes de que tenemos en Guatemala. Tener piel mixta significa que en el área de la zona T (frente y nariz) tenemos grasa, y en todo lo demás del rostro, tendemos a tener la piel seca. "
                />
            </div>
        </div>
    );
}

export default MakeupSkin;