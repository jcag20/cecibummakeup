import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function NavItem(props){
    return(
        <div className="navbar-item">
            <a href={props.togo} className="navbar-link">
                {/* <i><FontAwesomeIcon icon={faEllipsisV} /></i> */}
                <span>{props.section}</span>
            </a>
        </div>
    );
}

export default NavItem;