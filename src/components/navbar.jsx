import React from 'react';
import NavItem from './navbar-item';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faBars} from '@fortawesome/free-solid-svg-icons';

function NavBar(){
    return(
        <div className="navbar">
            <i><FontAwesomeIcon icon={faBars} /></i>
            <NavItem togo="#about" section="Acerca de mí"/>
            <NavItem togo="#makeupskin"section="Makeup" />
            <NavItem togo="#makeupskin"section="Skincare" />
            <NavItem togo="#contact"section="Contáctame" />
        </div>
 
    );
}

export default NavBar;