import React from 'react';
import './App.css';
import Portada from './components/portada';
import About from './components/about';
import MakeupSkin from './components/makeupskin';
import NavBar from './components/navbar';
import Contact from './components/contact';

function App() {
  return (
    <div>
      <NavBar />
      <Portada />
      <About />
      <MakeupSkin />
      <Contact />
    </div>
      
  );
}

export default App;
